from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.conf import settings
from django.core.mail import EmailMessage

"""
mailed = send_mail(
            mail_entry['title'],  # change this to custom email subject lines
            msg_html,
            self.fio_email,
            [recipient],
            html_message=msg_html,
            fail_silently=False
        )
"""


MAIL_INDEX = {
    # NHM  notifications
    'ACCESS.REQUEST': {
        'template': "notifications/access.request.html",
        'recipient': 'charity',
        'title': 'A New Provider has Requested Access',
        'context': {'organization_name': 'n/a',
                    'email': 'n/a',
                    'phone': 'n/a',
                    'contact_email': 'n/a'}
    },
    'PROVIDER.WELCOME': {
        'template': "notifications/provider.welcome.html",
        'recipient': 'charity',
        'title': "Congratulations! You've been granted access to Nashville Hearts Me",
        'context': {'organization': 'n/a',
                    'email': 'n/a',
                    'password': 'n/a',
                    'contact_name': 'n/a'}
    },
    'B.5': {
        'template': "notifications/admin/B.5.html",
        'recipient': 'admin',
        'title': 'ACTION ITEM: Need has been published',
        'context': {'charity_name': 'n/a', 'charity_ein': 'n/a',
                    'charity_contact_fullname': 'n/a', 'charity_contact_email': 'n/a',
                    'need_title': 'n/a', 'need_type': 'n/a', 'need_amount': 0.0,
                    'need_description': 'n/a'}
    },
}


class MailGate(object):

    def __init__(self):
        self.nhm_email = "Nashville Hearts Me <webmaster@siggyworks.com>"  # or tech@forwarditon.org

    def sendmail(self, email_code, recipient, context=None, request=None):
        """
        :param email_code:
        :type email_code: string
        :param recipient:
        :type recipient: recipient email (string)
        :param context:
        :type context: dictionary object {'charity': charity_context, 'donor': donor_context, etc...}
        :param request:
        :type request: HttpRequest Object from Django
        :return:
        :rtype:
        """

        if not email_code:
            return False, email_code

        if not recipient:
            return False, recipient

        mail_entry = MAIL_INDEX[email_code]

        if context:
            mail_entry['context'].update(context)
            # update the received context with 'domain_protocol' i.e., http or https
            protocol = "http://" if settings.NHM_TEST_MODE is True else "https://"
            mail_entry['context'].update({'protocol': protocol})

        if request and hasattr(request, 'get_host'):
            req = {
                'request': request,
                'domain_host': request.get_host()
            }
            mail_entry['context'].update(req)

        msg_html = render_to_string(mail_entry['template'], mail_entry['context'])
        email = EmailMessage(mail_entry['title'], msg_html, self.nhm_email, [recipient])
        email.content_subtype = "html"
        mailed = email.send()

        # print(mailed)
        # print(mail_entry['context'])
        return mailed, "Mail Sent?"

    def send_contact_email(self, email, message="Message body was empty!", name="Anonymous", subject=None):
        subject = "Contact Message From Nashville Hearts Me" if subject is None else subject
        mailed = send_mail(
            subject,
            message,
            from_email="{} <{}>".format(name, email),
            recipient_list=[settings.NHM_ADMIN_EMAIL],
            fail_silently=False
        )
        return mailed, "Contact Email Sent?"

    def send_maintenance_email(self, message="Message body was empty!", email=None, subject=None):
        email = "webmaster@siggyworks.com" if email is None else email
        subject = "Maintenance Notification From NHM" if subject is None else subject
        domain = settings.NHM_ROOT_URL if settings else None
        if domain and domain.find("//") > -1:
            _, domain = settings.FIO_ROOT_URL.split("//", 1)
        mailed = send_mail(
            subject,
            message,
            from_email="NHM Admin ({}) <{}>".format(domain, "webmaster@siggyworks.com"),
            recipient_list=[email],
            fail_silently=False
        )
        return mailed, "Maintenance Email Sent?"

    def send_test_email(self):
        msg_html = render_to_string("notifications/F.1.html", {'first_name': 'Sada', 'charity_name': 'Nashville Hearts'})
        mailed = send_mail(
            "NHM Test Email",  # change this to custom email subject lines
            "This is a test email",
            "webmaster@siggyworks.com",
            ["syghon@gmail.com"],
            html_message=msg_html,
            fail_silently=False
        )
        return mailed, "Test Mail Sent?"
