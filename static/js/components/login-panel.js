/**
 * Created by sada on 12/30/16.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import Subheader from 'material-ui/Subheader';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

const styles = {
    wrapper: {
        padding: "20px 10px 10px",
        textAlign: "center",
        color: '#777777'
    },
    subheader: {
        color: '#676767'
    },
    textfield: {
        color: '#777777'
    }
};

export default class LoginPanel extends React.Component {

    constructor(props, context) {
        super(props, context);
    }


    render() {
        return (
            <div style={styles.wrapper}>
                <Subheader style={styles.subheader}>Provider Account Login</Subheader>
                <TextField style={styles.textfield} hintText="Provider Email Address" floatingLabelText="Email" type="email"/><br/>
                <TextField style={styles.textfield} hintText="Password" floatingLabelText="Password" type="password"/><br/><br/>
                <RaisedButton label="Login" fullWidth={true} primary={true} />
            </div>
        );
    }

    componentDidMount() {
        console.log("...LoginPanel DidMount...");
    }

    componentDidUpdate(prevProps) {
        console.log("...LoginPanel DidUpdate...");
    }

    componentWillUnmount() {
        console.log("...LoginPanel WillUnmount...");
    }

    handleEvent(evtName) {
        let handlerName = "on"+camelize(evtName);

        // call the handler which should be on the props object
        return (e) => {
            if(this.props[handlerName]) {
                this.props[handlerName](this.props, this.marker, e);
            } else {
                console.log('Could not find handler: ' + handlerName);
            }
        };
    }
}

// LoginPanel.defaultProps = {};

// Helper functions
        function camelize(str) {
            return str.split(' ').map(function(word){
                return word.charAt(0).toUpperCase() + word.slice(1);
            }).join('');
        }
