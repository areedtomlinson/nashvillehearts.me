/**
 * Created by sada on 10/11/16.
 */

import React from 'react';
import Drawer from 'material-ui/Drawer';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import LoginPanel from './components/login-panel';

const styles = {
	bars: {
		backgroundColor: '#282C35',
        textColor: '#22ABD7',
        fontSize: 16
	},
    tooltitle: {
	    color: '#22ABD7',
    }
};

class MainLeftDrawer extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = { open: this.props.open };
    }
    doToggleDrawer = () => {
        this.setState({
            open: !this.state.open
        });
    }
    render() {

        return (
            <Drawer open={this.state.open} width={280}>
                <Toolbar>
                    <ToolbarGroup>
                        <ToolbarTitle style={styles.tooltitle} text={this.props.title} />
                    </ToolbarGroup>
                    <ToolbarGroup lastChild={true}>
                        <IconButton onTouchTap={this.doToggleDrawer}><NavigationClose /></IconButton>
                    </ToolbarGroup>
                </Toolbar>
                <LoginPanel/>
            </Drawer>
        );

    }
}

MainLeftDrawer.defaultProps = { title: "Providers", open: false };

export default MainLeftDrawer;
