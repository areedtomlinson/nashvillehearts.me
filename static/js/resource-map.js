/**
 * Created by sada on 12/27/16.
 */

import React from 'react';
import GoogleAPIWrapper from './utils/GoogleApiComponent';
import Map from './components/map';
import Marker from './components/marker';
import InfoWindow from './components/info-window';
import ModalInfoWindow from './components/modal-info-window';


export class ResourceMap extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            data: null, // == 'provider' | 'event' | 'need'
            showingInfoWindow: false,
            showingModalInfoWindow: false,
            activeMarker: {},
            selectedLocation: {}
        };

        this.test_coordinates = [
            { lat: 36.1582766, lng: -86.7699213 },  //pinewood
            { lat: 36.1431687, lng: -86.7692093 },  //refinery
            { lat: 36.1640722, lng: -86.7996149 }   //marathon
        ];
    }

    onMarkerMouseover(props, marker, e) {
        this.setState({
            selectedLocation: props,
            activeMarker: marker,
            showingInfoWindow: true
        });
    }

    onMapClick() {
        if (this.state && this.state.showingInfoWindow) {
            this.setState({
                showingInfoWindow: false,
                activeMarker: null
            });
        }
    }

    onInfoWindowClose() {
        this.setState({
            showingInfoWindow: false,
            activeMarker: null
        });
    }

    parseProviderLocations() {
		var locations = [];
		console.log('...in parseProviderLocations...');
        console.log(this.props.data);
		if(this.props && this.props.data) {
		    for(let i=0; i < this.props.data.length; i++) {
		        let provider = this.props.data[i];
                let temp = provider.locations.map( (loc, index) => {
                    return (
                        <Marker key={index} onMouseOver={this.onMarkerMouseover}
                                name={loc.name} position={ {lat: loc.latitude, lng: loc.longitude } } />
                    );
                } );

                locations = locations.concat(temp);
            }
        }

        console.log(locations);

        return locations;
	}

    render() {
        const style = {
            width: '100vw',
            height: '100vh'
        };

        /*
        if (!this.props.loaded) {
            return <div style={style}>Loading...</div>
        }

        ...render markers here...
        <Marker onMouseOver={this.onMarkerMouseover} name={'No Name'} />
                    <Marker onMouseOver={this.onMarkerMouseover}
                            name={'Refinery Nashville'} position={this.test_coordinates[1]} />
                    <Marker onMouseOver={this.onMarkerMouseover}
                            name={'Marathon Music Works'} position={this.test_coordinates[2]} />
        */

        let providerLocations = this.parseProviderLocations();

        return (
            <div style={style}>
                <Map google={this.props.google} onClick={this.onMapClick}>

                    {providerLocations}

                    <InfoWindow marker={this.state.activeMarker}
                                visible={this.state.showingInfoWindow}
                                onClose={this.onInfoWindowClose}>
                        <div className="label-info">
                            <h4>{this.state.selectedLocation.name}</h4>
                        </div>
                    </InfoWindow>
                </Map>
                <ModalInfoWindow marker={this.state.activeMarker}
                                 visible={this.state.showingModalInfoWindow}
                                 location={this.state.selectedLocation}/>
            </div>
        );
    }
}


export default GoogleAPIWrapper({
  apiKey: 'AIzaSyB6L-gd4MueuPig0CtU6He3nf9lebyfYwI'
})(ResourceMap)

//export default ResourceMap;
