from .common import *
from django.utils.timezone import activate


# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases
DB_NAME = env_keys.get_variable('DB_NAME')
DB_USER = env_keys.get_variable('DB_USER')
DB_PASS = env_keys.get_variable('DB_PASS')
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': DB_NAME,
        'USER': DB_USER,
        'PASSWORD': DB_PASS,
    }
}

TIME_ZONE = 'America/Chicago'
activate(TIME_ZONE)

NHM_ADMIN_EMAIL = 'webmaster@siggyworks.com'

NHM_TEST_MODE = True

NHM_ROOT_URL = "nmhrc.platform.siggyworks.com"
# NHM_ROOT_URL = "104.236.119.135"
# NHM_ROOT_URL = "127.0.0.1:8000"

NHM_WEB_CLIENT_URL = "nashvillehearts.me"
# NHM_WEB_CLIENT_URL = "nashvilleheartsme.siggyworks.com"

